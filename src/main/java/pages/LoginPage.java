package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	
	public MiBrandPage mouseHoverOnElectronics() throws InterruptedException {
		WebElement ele1 = locateElement("xpath", "//span[text()='Electronics']");
		WebElement ele2 = locateElement("xpath", "//a[text()='Mi']");
		mouseAction(ele1, ele2);
		Thread.sleep(2000);
		verifyTitle("Mi Mobile Phones");
		return new MiBrandPage();
		
	}
	
	
}