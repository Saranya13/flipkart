package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class RedMiPage extends ProjectMethods {
	
	public RedMiPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//div[@class='_3wU53n']") WebElement eleClckFirstProduct;
	public RedMiPage clckFirstProduct() {
		text = eleClckFirstProduct.getText();
		System.out.println(text);
		click(eleClckFirstProduct);
		return this;
	}
	
	public RedMiPage verifyTitleOfProductPage() {
		switchToWindow(1);
		verifyTitle(text);
		return this;

	}
	
	@FindBy(className="_38sUEc") WebElement eleREviews;
	public RedMiPage verifyRew() {
		System.out.println(eleREviews.getText());
		verifyPartialText(eleREviews, "Ratings");
		return this;
	}
}
