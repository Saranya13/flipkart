package testcases;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.MiBrandPage;
import wdMethods.ProjectMethods;

public class TC002_MiBrand extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_MiBrand";
		testDescription = "LoginAndLogout";
		authors = "sarath";
		category = "smoke";
		testNodes = "Leads";
	}
	
	@Test
	public void miBrand() throws InterruptedException {
		new LoginPage()
		.mouseHoverOnElectronics()
		.clickNewestFirst()
		.printProductNameAndPrice()
		.clckFirstProduct()
		.verifyTitleOfProductPage()
		.verifyRew();
		
	}
	
	
	
	
}
